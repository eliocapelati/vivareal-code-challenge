# Implementação desafio backend - Vivareal code-challenge


## Sobre a implementação

O desafio foi implementado utilizando a tecnologia Java, com a utilização do framework Spring Boot.

## Como executar a aplicação

A aplicação utiliza de uma ferramenta de gerenciamento de dependências, e através dela, por meio de convenções seguidas na organização do código fonte, é possivel que as dependências sejam baixadas da internet, que o código fonte seja compilado, e os testes sejam executados, bem como é possível que a aplicação esteja rodando com apenas alguns comandos.

Essa ferramenta de gerenciamento de código se chama Gradle, e pode ser executada tanto em Linux/Mac (gradlew) tanto quanto em windows (gradle.bat) bastando apenas ter o JDK 1.8+ instalado na máquina.

### Primeira execução

Para que seja possível executar o projeto, basta abrir seu Terminal ou Prompt de comando disponível em seu sistema operacional, e executar o comando a seguir:

Unix based:
```sh
$ ./gradlew build
```

Windows
```sh
$ gradle.bat build
```

Este comando durante sua primeira execução pode demorar algum tempo, ele está baixando as dependencias da internet (Na segunda execução em diante, esse processo será muito mais rápido, pois o Gradle criará um cache local)!

### Colocando a aplicação no ar 

Por conveniência de já estarmos familiarizados com o Gradle, também utilizaremos ele para rodar a aplicação, mas o processo de um ambiente produtivo é ligeiramente diferente, e será explicado na sequência.

Para que possamos ver a api no ar, precisamos rodar o seguinte comando no terminal:

Unix based:
```sh
$ ./gradlew bootRun
```

Windows
```sh
$ gradle.bat bootRun
```

Assim que a mensagem de startup aparecer, podemos começar usar nossa API. Uma sequência de mensagens de log apareceram no terminal, e uma mensagem parecida com essa "Started SpotipposApplication in 26.246 seconds" indicará que o ambiente está pronto!


## Consumindo a API

Com a aplicação no ar, podemos começar a utilizar a API, ou seja, Criar, alterar, excluir, e consultar por quadrante.

Utilize o seguinte endereço base para começar: http://localhost:8080/properties

Para criar basta utilizar alguma ferramenta que facilite esse trabalho, caso esteja utilizando Google Chrome, poderá instalar o Postman.

### Criando uma property

POST http://localhost:8080/properties

Body:

```json
{
  "x": 222,
  "y": 444,
  "title": "Imóvel código 1, com 5 quartos e 4 banheiros",
  "price": 1250000,
  "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  "beds": 4,
  "baths": 3,
  "squareMeters": 210
}
```

Response:
 
Header:
HTTP/1.1 201
Content-Encoding: gzip
Content-Type: application/json;charset=UTF-8
Date: Wed, 07 Sep 2016 08:29:23 GMT
Location: http://localhost:8080/properties/8002
Transfer-Encoding: chunked
Vary: Accept-Encoding

Body:

```json
{
  "id": 8001,
  "title": "Imóvel código 1, com 5 quartos e 4 banheiros",
  "price": 1250000,
  "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  "x": 222,
  "y": 444,
  "beds": 4,
  "baths": 3,
  "provinces": [],
  "squareMeters": 210,
  "lat": 222,
  "long": 444,
  "_links": {
    "self": {
      "href": "http://localhost:8080/properties/8001"
    },
    "property": {
      "href": "http://localhost:8080/properties/8001"
    }
  }
}
```

### Buscando uma property existente


GET http://localhost:8080/properties/1

Response:

Header:

HTTP/1.1 200
Content-Encoding: gzip
Content-Type: application/hal+json;charset=UTF-8
Date: Wed, 07 Sep 2016 08:31:46 GMT
Transfer-Encoding: chunked
Vary: Accept-Encoding


Body:

```json
{
    "_links": {
        "property": {
            "href": "http://localhost:8080/properties/1"
        },
        "self": {
            "href": "http://localhost:8080/properties/1"
        }
    },
    "baths": 2,
    "beds": 3,
    "description": "Laboris quis quis elit commodo eiusmod qui exercitation. In laborum fugiat quis minim occaecat id.",
    "id": 1,
    "lat": 1257,
    "long": 928,
    "price": 643000.0,
    "provinces": [
        "Jaby"
    ],
    "squareMeters": 61,
    "title": "Imóvel código 1, com 3 quartos e 2 banheiros.",
    "x": 1257,
    "y": 928
}
```

### Buscando imóveis em Spotippos


GET http://localhost:8080/properties/search/coord?ax=200&ay=250&bx=400&by=500

Response:

Header:

HTTP/1.1 200
Content-Encoding: gzip
Content-Type: application/hal+json;charset=UTF-8
Date: Wed, 07 Sep 2016 08:34:45 GMT
Transfer-Encoding: chunked
Vary: Accept-Encoding

Body:

```json
{
    "_embedded": {
        "properties": [
            {
                "_links": {
                    "property": {
                        "href": "http://localhost:8080/properties/921"
                    },
                    "self": {
                        "href": "http://localhost:8080/properties/921"
                    }
                },
                "baths": 4,
                "beds": 5,
                "description": "Anim laborum consequat labore reprehenderit sit amet irure incididunt. Sunt magna nisi pariatur dolore proident incididunt ex exercitation pariatur adipisicing et culpa.",
                "id": 921,
                "lat": 223,
                "long": 488,
                "price": 1859000.0,
                "provinces": [
                    "Scavy"
                ],
                "squareMeters": 182,
                "title": "Imóvel código 921, com 5 quartos e 4 banheiros.",
                "x": 223,
                "y": 488
            },
            {
                "_links": {
                    "property": {
                        "href": "http://localhost:8080/properties/1162"
                    },
                    "self": {
                        "href": "http://localhost:8080/properties/1162"
                    }
                },
                "baths": 4,
                "beds": 5,
                "description": "Magna esse aute do cillum mollit adipisicing ut irure laborum eiusmod esse veniam duis. Incididunt eiusmod anim reprehenderit Lorem ad excepteur amet nisi labore commodo eu.",
                "id": 1162,
                "lat": 205,
                "long": 407,
                "price": 2021000.0,
                "provinces": [
                    "Scavy"
                ],
                "squareMeters": 198,
                "title": "Imóvel código 1162, com 5 quartos e 4 banheiros.",
                "x": 205,
                "y": 407
            },
            ...
        ]
    },
    "_links": {
        "first": {
            "href": "http://localhost:8080/properties/search/coord?ax=200&ay=250&bx=400&by=500&page=0&size=20"
        },
        "last": {
            "href": "http://localhost:8080/properties/search/coord?ax=200&ay=250&bx=400&by=500&page=2&size=20"
        },
        "next": {
            "href": "http://localhost:8080/properties/search/coord?ax=200&ay=250&bx=400&by=500&page=1&size=20"
        },
        "self": {
            "href": "http://localhost:8080/properties/search/coord?ax=200&ay=250&bx=400&by=500"
        }
    },
    "page": {
        "number": 0,
        "size": 20,
        "totalElements": 42,
        "totalPages": 3
    }
}
```


#### Formato da busca

Como pode ser visto, o formato da busca está ligeiramente diferente tanto na URI, quanto no json de resposta.
Essa decisão foi tomada para seguir o padrão de API [HAL (Hypertext Application Language)](http://stateless.co/hal_specification.html), o que torna a API explorável.


## Colocando a aplicação em produção

Para que seja possivel rodar a aplicação em um servidor dedicado, basta apenas distribuir o arquivo Spotippos-1.0.0.jar que estará disponível na pasta build/libs após o processo de build com Gradle.

Isto feito agora é hora de executar com o comando abaixo, e sua aplicação estará disponível na porta 8080:

```sh
$ java -jar Spotippos-1.0.0.jar
```
