
package br.com.vivareal.codechallenge.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "totalProperties",
    "properties"
})
/**
 * Used for load from json file
 * @author eliocapelati
 *
 */
public class Spotippos {

    @JsonProperty("totalProperties")
    private Long totalProperties;
    @JsonProperty("properties")
    @Valid
    private List<Property> properties = new ArrayList<Property>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Spotippos() {
    }

    /**
     * 
     * @param properties
     * @param totalProperties
     */
    public Spotippos(Long totalProperties, List<Property> properties) {
        this.totalProperties = totalProperties;
        this.properties = properties;
    }

    /**
     * 
     * @return
     *     The totalProperties
     */
    @JsonProperty("totalProperties")
    public Long getTotalProperties() {
        return totalProperties;
    }

    /**
     * 
     * @param totalProperties
     *     The totalProperties
     */
    @JsonProperty("totalProperties")
    public void setTotalProperties(Long totalProperties) {
        this.totalProperties = totalProperties;
    }

    public Spotippos withTotalProperties(Long totalProperties) {
        this.totalProperties = totalProperties;
        return this;
    }

    /**
     * 
     * @return
     *     The properties
     */
    @JsonProperty("properties")
    public List<Property> getProperties() {
        return properties;
    }

    /**
     * 
     * @param properties
     *     The properties
     */
    @JsonProperty("properties")
    public void setProperties(List<Property> properties) {
        this.properties = properties;
    }

    public Spotippos withProperties(List<Property> properties) {
        this.properties = properties;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Spotippos withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(totalProperties).append(properties).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Spotippos) == false) {
            return false;
        }
        Spotippos rhs = ((Spotippos) other);
        return new EqualsBuilder().append(totalProperties, rhs.totalProperties).append(properties, rhs.properties).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
