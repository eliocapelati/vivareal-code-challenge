package br.com.vivareal.codechallenge.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;


/**
 * 
 * @author eliocapelati
 *
 */
@Entity
@Table(name = "province")
public class Province implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2038184574614278338L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long province_id;
	private String name;
	private Integer upperLeftX;
	private Integer upperLeftY;
	private Integer bottomRightX;
	private Integer bottomRightY;
	

	/**
	 * 
	 */
	public Province() {
	}

	/**
	 * 
	 * @param name
	 * @param upperLeftX
	 * @param upperLeftY
	 * @param bottomRightX
	 * @param bottomRightY
	 */
	public Province(String name, Integer upperLeftX, Integer upperLeftY, Integer bottomRightX, Integer bottomRightY) {
		this.name = name;
		this.upperLeftX = upperLeftX;
		this.upperLeftY = upperLeftY;
		this.bottomRightX = bottomRightX;
		this.bottomRightY = bottomRightY;
	}
	
	/**
	 * 
	 * @return
	 */
	public Long getProvinceId() {
		return province_id;
	}
	/**
	 * 
	 * @param province_id
	 */
	public void setProvinceId(Long id) {
		this.province_id = id;
	}
	public Province withProvinceId(Long id) {
		this.province_id = id;
		return this;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @param name
	 * @return this
	 */
	public Province withName(String name) {
		this.name = name;
		return this;
	}

	/**
	 * @return the upperLeftX
	 */
	public Integer getUpperLeftX() {
		return upperLeftX;
	}

	/**
	 * @param upperLeftX
	 *            the upperLeftX to set
	 */
	public void setUpperLeftX(Integer upperLeftX) {
		this.upperLeftX = upperLeftX;
	}

	/**
	 * 
	 * @param upperLeftX
	 * @return
	 */
	public Province withUpperLeftX(Integer upperLeftX) {
		this.upperLeftX = upperLeftX;
		return this;
	}

	/**
	 * @return the upperLeftY
	 */
	public Integer getUpperLeftY() {
		return upperLeftY;
	}

	/**
	 * @param upperLeftY
	 *            the upperLeftY to set
	 */
	public void setUpperLeftY(Integer upperLeftY) {
		this.upperLeftY = upperLeftY;
	}

	/**
	 * 
	 * @param upperLeftY
	 * @return
	 */
	public Province withUpperLeftY(Integer upperLeftY) {
		this.upperLeftY = upperLeftY;
		return this;
	}

	/**
	 * @return the bottomRightX
	 */
	public Integer getBottomRightX() {
		return bottomRightX;
	}

	/**
	 * @param bottomRightX
	 *            the bottomRightX to set
	 */
	public void setBottomRightX(Integer bottomRightX) {
		this.bottomRightX = bottomRightX;
	}

	/**
	 * 
	 * @param bottomRightX
	 * @return
	 */
	public Province withBottomRightX(Integer bottomRightX) {
		this.bottomRightX = bottomRightX;
		return this;
	}

	/**
	 * @return the bottomRightY
	 */
	public Integer getBottomRightY() {
		return bottomRightY;
	}

	/**
	 * @param bottomRightY
	 *            the bottomRightY to set
	 */
	public void setBottomRightY(Integer bottomRightY) {
		this.bottomRightY = bottomRightY;
	}

	/**
	 * 
	 * @param bottomRightY
	 * @return
	 */
	public Province withBottomRightY(Integer bottomRightY) {
		this.bottomRightY = bottomRightY;
		return this;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(name).append(bottomRightX).append(bottomRightY).append(upperLeftX)
				.append(upperLeftY).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof Province) == false) {
			return false;
		}
		Province rhs = ((Province) other);
		return new EqualsBuilder().append(name, rhs.name).append(bottomRightX, rhs.bottomRightX)
				.append(bottomRightY, rhs.bottomRightY).append(upperLeftX, rhs.upperLeftX)
				.append(upperLeftY, rhs.upperLeftY).isEquals();
	}

}
