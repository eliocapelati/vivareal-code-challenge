
package br.com.vivareal.codechallenge.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.Range;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import br.com.vivareal.codechallenge.listener.PropertyEntityListener;


/**
 * 
 * @author eliocapelati
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "title",
    "price",
    "description",
    "x",
    "y",
    "beds",
    "baths",
    "provinces",
    "squareMeters"
})
@Entity
@EntityListeners({PropertyEntityListener.class})
@Table(name="property")
public class Property implements Serializable {

    
	/**
	 * 
	 */
	private static final long serialVersionUID = 2853740436136539300L;

	@JsonProperty("id")
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
	
	@Size(min=0, max=50)
    @JsonProperty("title")
    private String title;
    
    @JsonProperty("price")
    private BigDecimal price;
    
    @JsonProperty("description")
    private String description;
    
    @Range(min=0, max=1400)
    @JsonProperty("x")
    private Integer lat;
    
    @Range(min=0, max=1000) 
    @JsonProperty("y")
    private Integer lng;
    
	@Range(min=1, max=5)
    @JsonProperty("beds")
    private Long beds;
    
    @Range(min=1, max=4)
    @JsonProperty("baths")
    private Long baths;
    
    @JsonProperty("provinces")
    @Valid
    @Transient
    private List<String> provinces = new ArrayList<String>();
    
    @Range(min=20, max=240)
    @JsonProperty("squareMeters")
    private Long squareMeters;
    
    @Transient
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Property() {
    }

    /**
     * 
     * @param id
     * @param title
     * @param price
     * @param description
     * @param lat
     * @param lng
     * @param beds
     * @param baths
     * @param provinces
     * @param squareMeters
     */
    public Property(Long id, String title, BigDecimal price, String description, Integer lat, Integer lng, Long beds, Long baths, List<String> provinces, Long squareMeters) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.description = description;
        this.lat = lat;
        this.lng = lng;
        this.beds = beds;
        this.baths = baths;
        this.provinces = provinces;
        this.squareMeters = squareMeters;
    }

    /**
     * 
     * @return
     *     The id
     */
    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    public Property withId(Long id) {
        this.id = id;
        return this;
    }

    /**
     * 
     * @return
     *     The title
     */
    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    public Property withTitle(String title) {
        this.title = title;
        return this;
    }

    /**
     * 
     * @return
     *     The price
     */
    @JsonProperty("price")
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * 
     * @param price
     *     The price
     */
    @JsonProperty("price")
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Property withPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    /**
     * 
     * @return
     *     The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public Property withDescription(String description) {
        this.description = description;
        return this;
    }

    /**
     * 
     * @return
     *     The lat
     */
    @JsonProperty("lat")
    public Integer getLat() {
        return lat;
    }

    /**
     * 
     * @param lat
     *     The lat
     */
    @JsonProperty("lat")
    public void setLat(Integer lat) {
        this.lat = lat;
    }

    public Property withLat(Integer lat) {
        this.lat = lat;
        return this;
    }

    /**
     * 
     * @return
     *     The lng
     */
    @JsonProperty("long")
    public Integer getLng() {
        return lng;
    }

    /**
     * 
     * @param lng
     *     The lng
     */
    @JsonProperty("long")
    public void setLng(Integer lng) {
        this.lng = lng;
    }

    public Property withLng(Integer lng) {
        this.lng = lng;
        return this;
    }

    /**
     * 
     * @return
     *     The beds
     */
    @JsonProperty("beds")
    public Long getBeds() {
        return beds;
    }

    /**
     * 
     * @param beds
     *     The beds
     */
    @JsonProperty("beds")
    public void setBeds(Long beds) {
        this.beds = beds;
    }

    public Property withBeds(Long beds) {
        this.beds = beds;
        return this;
    }

    /**
     * 
     * @return
     *     The baths
     */
    @JsonProperty("baths")
    public Long getBaths() {
        return baths;
    }

    /**
     * 
     * @param baths
     *     The baths
     */
    @JsonProperty("baths")
    public void setBaths(Long baths) {
        this.baths = baths;
    }

    public Property withBaths(Long baths) {
        this.baths = baths;
        return this;
    }

    /**
     * 
     * @return
     *     The provinces
     */
    @JsonProperty("provinces")
    public List<String> getProvinces() {
        return provinces;
    }

    /**
     * 
     * @param provinces
     *     The provinces
     */
    @JsonProperty("provinces")
    public void setProvinces(List<String> provinces) {
        this.provinces = provinces;
    }

    public Property withProvinces(List<String> provinces) {
        this.provinces = provinces;
        return this;
    }

    /**
     * 
     * @return
     *     The squareMeters
     */
    @JsonProperty("squareMeters")
    public Long getSquareMeters() {
        return squareMeters;
    }

    /**
     * 
     * @param squareMeters
     *     The squareMeters
     */
    @JsonProperty("squareMeters")
    public void setSquareMeters(Long squareMeters) {
        this.squareMeters = squareMeters;
    }

    public Property withSquareMeters(Long squareMeters) {
        this.squareMeters = squareMeters;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Property withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).append(title).append(price).append(description)
        		.append(lat).append(lng)
        		.append(beds).append(baths).append(provinces).append(squareMeters).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Property) == false) {
            return false;
        }
        Property rhs = ((Property) other);
        return new EqualsBuilder().append(id, rhs.id).append(title, rhs.title).append(price, rhs.price).append(description, rhs.description)
        		.append(lat, rhs.lat).append(lng, rhs.lng)
        		.append(beds, rhs.beds).append(baths, rhs.baths).append(provinces, rhs.provinces).append(squareMeters, rhs.squareMeters).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
