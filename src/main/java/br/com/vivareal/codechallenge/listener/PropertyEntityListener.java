package br.com.vivareal.codechallenge.listener;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.PostLoad;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.vivareal.codechallenge.config.AutowireContext;
import br.com.vivareal.codechallenge.domain.Property;
import br.com.vivareal.codechallenge.domain.Province;
import br.com.vivareal.codechallenge.repository.ProvinceRepository;

/**
 * 
 * @author eliocapelati
 *
 */
public class PropertyEntityListener {
	private static final Logger LOG = LoggerFactory.getLogger(PropertyEntityListener.class);

	@Autowired
	private ProvinceRepository province;

	public PropertyEntityListener() {
	}

	/**
	 * Populate the provinces a @{link Property} on querying.
	 * @param entity
	 */
	@PostLoad
	public void determineProvince(final Property entity) {
		AutowireContext.autowire(this, province);
		
		List<String> provinces = province
									.findByCoordinates(entity.getLat(), entity.getLng())
									.stream()
									.map(Province::getName)
									.collect(Collectors.toList());
		
		
		
		entity.setProvinces(provinces);
		LOG.debug("Property : {}", entity);
	}

}
