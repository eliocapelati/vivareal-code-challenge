package br.com.vivareal.codechallenge.exception;

/**
 * 
 * @author eliocapelati
 *
 */
public class AppBootException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6189046132617917606L;
	
	/**
	 * 
	 */
	public AppBootException() {
		super();
	}
	
	/**
	 * 
	 * @param message
	 */
	public AppBootException(String message){
		super(message);
	}
	
	/**
	 * 
	 * @param message
	 * @param cause
	 */
	public AppBootException(String message, Throwable cause) {
        super(message, cause);
    }
	
	/**
	 * 
	 * @param cause
	 */
	public AppBootException(Throwable cause) {
        super(cause);
    }

}
