package br.com.vivareal.codechallenge.boot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import br.com.vivareal.codechallenge.load.LoadData;


/**
 * Load Json from storage and populate database.
 * @author eliocapelati
 *
 */
@Component
public class AppBootstrap implements ApplicationListener<ApplicationReadyEvent> {
		
	private LoadData load;
	
	@Autowired
	public AppBootstrap(LoadData load) {
		super();
		this.load = load;
	}

	@Override
	public void onApplicationEvent(final ApplicationReadyEvent event) {
		load.loadPropertiesToDatabase();
	}

}
