package br.com.vivareal.codechallenge.repository;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

import br.com.vivareal.codechallenge.domain.Province;

/**
 * 
 * @author eliocapelati
 *
 */
@RestResource(exported=false)
public interface ProvinceRepository extends JpaRepository<Province, Long> {
	
	/**
	 * Find a Province based on coordinates.
	 * @param lat
	 * @param lng
	 * @return
	 */
	@Cacheable("provinces")
	@Query("SELECT p FROM Province p WHERE (:lat BETWEEN p.upperLeftX AND p.bottomRightX) AND (:lng BETWEEN  p.bottomRightY AND p.upperLeftY)")
	List<Province> findByCoordinates(@Param("lat") Integer lat,@Param("lng") Integer lng);
	
}
