package br.com.vivareal.codechallenge.load;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.vivareal.codechallenge.domain.Province;
import br.com.vivareal.codechallenge.domain.Spotippos;
import br.com.vivareal.codechallenge.exception.AppBootException;
import br.com.vivareal.codechallenge.repository.PropertyRepository;
import br.com.vivareal.codechallenge.repository.ProvinceRepository;

/**
 * 
 * @author eliocapelati
 *
 */
@Component
public class LoadData {
	
	private static final Logger LOG = LoggerFactory.getLogger(LoadData.class);
    private PropertyRepository property;
    private ProvinceRepository province;
    
    @Autowired
	public LoadData(PropertyRepository property, ProvinceRepository province) {
		this.property = property;
		this.province = province;
	}
	
	@Value("${br.com.vivareal.codechallenge.properties.json.load.location}")
	private String propertiesLocation;
	@Value("${br.com.vivareal.codechallenge.province.json.load.location}")
	private String provinceLocation;

	/**
	 * 
	 * @throws AppBootException
	 */
	public void loadPropertiesToDatabase() throws AppBootException {
		LOG.debug("Load Data - LOAD FILES RESOURCES - INIT");
		LOG.debug("Load PROPERTIES");
		loadProvincesToDatabase();		
		ObjectMapper mapper = new ObjectMapper();
		Spotippos spo = new Spotippos();
		try {
			spo = mapper.readValue(new File(propertiesLocation), Spotippos.class);
		} catch (IOException e) {
			throw new AppBootException("Cant start APP, Properties cant be load", e);
		}
		property.save(spo.getProperties());
		LOG.debug("App Startup - LOAD FILES RESOURCES - END");
	}
	
	/**
	 * 
	 * @throws AppBootException
	 */
	public void loadProvincesToDatabase() throws AppBootException {
		LOG.debug("Load PROVINCES");
		ObjectMapper mapper = new ObjectMapper();
		
		JsonNode jsonNode = null;
		try {
			jsonNode = mapper.readTree(new File(provinceLocation));
		} catch (IOException e) {
			throw new AppBootException("Cant start APP, Provinces cant be load", e);
		}

		jsonNode.fields().forEachRemaining(i -> {
			Province me = new Province().withName(i.getKey());

			JsonNode bound = i.getValue().get("boundaries");
			JsonNode uL    = bound.get("upperLeft");
			JsonNode bR    = bound.get("bottomRight");
			
			me.withUpperLeftX(uL.get("x").asInt())
			  .withUpperLeftY(uL.get("y").asInt())
			  .withBottomRightX(bR.get("x").asInt())
			  .withBottomRightY(bR.get("y").asInt());
			
			province.save(me);
		});
	}
}
