package br.com.vivareal.codechallenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * 
 * @author eliocapelati
 *
 */
@SpringBootApplication
@EnableCaching
public class SpotipposApplication {
		
	public static void main(String[] args) {
		SpringApplication.run(SpotipposApplication.class, args);
	}
}
