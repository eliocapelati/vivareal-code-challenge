package br.com.vivareal.codechallenge.config;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Used for non spring managed classes
 * 
 * @author eliocapelati
 *
 */
public final class AutowireContext implements ApplicationContextAware {

    private static final AutowireContext INSTANCE = new AutowireContext();
    private static ApplicationContext applicationContext;

    private AutowireContext() {
    }

    /**
     *
     * @param classReceiver the instance of the class which holds @Autowire annotations
     * @param beansToAutowireInClass the beans which have the @Autowire annotation in the specified classToAutowire
     */
    public static void autowire(Object classReceiver, Object ... beansToAutowireInClass) {
        for (Object bean : beansToAutowireInClass) {
            if (bean == null) {
                applicationContext.getAutowireCapableBeanFactory().autowireBean(classReceiver);
                return;
            }
        }
    }

    @Override
    public void setApplicationContext(final ApplicationContext applicationContext) {
        AutowireContext.applicationContext = applicationContext;
    }

    /**
     * @return the singleton instance.
     */
    public static AutowireContext getInstance() {
        return INSTANCE;
    }

}