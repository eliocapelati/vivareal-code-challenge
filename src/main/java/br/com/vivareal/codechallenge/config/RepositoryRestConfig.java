package br.com.vivareal.codechallenge.config;

import javax.persistence.Id;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;

import br.com.vivareal.codechallenge.domain.Property;

/**
 * When using {@link RepositoryRestResource} expose {@link Id} from Entity classes.
 * @author eliocapelati
 *
 */
@Configuration
public class RepositoryRestConfig extends RepositoryRestConfigurerAdapter {
	@Override
	public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
		config.exposeIdsFor(Property.class);
	}
}
