package br.com.vivareal.codechallenge.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

/**
 * Some app configurations
 * @author eliocapelati
 *
 */
@Configuration
public class AppConfig {
	
	/**
	 * Configura messageSource for custom messages on bean validation.
	 * @return
	 */
	@Bean(name = "messageSource")
	public ReloadableResourceBundleMessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageBundle = new ReloadableResourceBundleMessageSource();
		messageBundle.setBasename("classpath:messages/messages");
		messageBundle.setDefaultEncoding("UTF-8");
		return messageBundle;
	}
	
	/**
	 * Make spring available on non spring managed class
	 * @return
	 */
	@Bean
	public AutowireContext autowireContext(){
	    return AutowireContext.getInstance();
	}
}
