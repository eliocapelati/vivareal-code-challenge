package br.com.vivareal.codechallenge.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.event.ValidatingRepositoryEventListener;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

/**
 * Make bean validator messages correct on Rest error messages.
 * @author eliocapelati
 *
 */
@Configuration
public class RestValidatorMessageConfigAdapter extends RepositoryRestConfigurerAdapter {

	@Autowired
	private MessageSource messageSource;

	@Bean
	public Validator validator() {
		LocalValidatorFactoryBean factory = new LocalValidatorFactoryBean();
		factory.setValidationMessageSource(messageSource);
		return factory;
	}

	@Override
	public void configureValidatingRepositoryEventListener(ValidatingRepositoryEventListener validatingListener) {
		validatingListener.addValidator("afterCreate", validator());
		validatingListener.addValidator("beforeCreate", validator());
		validatingListener.addValidator("afterSave", validator());
		validatingListener.addValidator("beforeSave", validator());
	}

}
