package br.com.vivareal.codechallenge.repository;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.hasSize;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.vivareal.codechallenge.domain.Property;
import io.restassured.RestAssured;
import io.restassured.mapper.ObjectMapperType;

/**
 * 
 * @author eliocapelati
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties=
							{"br.com.vivareal.codechallenge.properties.json.load.location=src/test/resources/properties_1.json",
							 "br.com.vivareal.codechallenge.province.json.load.location=src/test/resources/provinces_full.json"})

public class PropertyRepositoryRestTest {


	private Property property;
	@Value("${local.server.port}") 
    int portServer;
	
	@Before
	public void setup(){
		RestAssured.port = portServer;
		
		property = new Property()
				.withBaths(3l)
				.withBeds(3l)
				.withDescription("with description")
				.withLat(1)
				.withLng(2)
				.withPrice(BigDecimal.ONE)
				.withSquareMeters(50l)
				.withTitle("Awesome test");
	}
	
	@Test
	public void testCreatePropertyShouldReturnCreated() throws Exception{
		
		given().log().all()
			.body(property, ObjectMapperType.JACKSON_2)
			.contentType("application/json")
		.expect()
			.statusCode(201) // created
	    .when()
	    	.post("/properties");
		
	}
	
	@Test
	public void testGetPropertyShouldReturnOk(){
		given().log().all()
		.expect()
			.statusCode(200) // OK
		.when()
			.get("/properties/1");
		
	}
	
	@Test
	public void testQueryByCoordinatesShouldReturnProperties(){
		given().log().all()
			.param("ax", 1)
			.param("ay", 250)
			.param("bx", 1)
			.param("by", 500)
		.expect()
			.statusCode(200) // OK
		.when()
			.get("/properties/search/coord")
		.then()
			.body("_embedded.properties", hasSize(1));
	}
}
