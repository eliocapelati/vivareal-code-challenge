/**
 * 
 */
package br.com.vivareal.codechallenge.repository;

import java.util.List;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.vivareal.codechallenge.domain.Property;
import br.com.vivareal.codechallenge.domain.Province;
import br.com.vivareal.codechallenge.repository.PropertyRepository;
import br.com.vivareal.codechallenge.repository.ProvinceRepository;

/**
 * @author eliocapelati
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties=
							{"br.com.vivareal.codechallenge.properties.json.load.location=src/test/resources/properties_1.json",
							 "br.com.vivareal.codechallenge.province.json.load.location=src/test/resources/provinces_full.json"})
public class ProvinceRepositoryTest {

	@Autowired
	private ProvinceRepository provinces;
	@Autowired
	private PropertyRepository property;
	
	
	@Test
	public void testFindByCoordinates() {
		List<Province> list = provinces.findByCoordinates(42, 42);
		assertEquals(1, list.size());
		assertEquals("Scavy", list.iterator().next().getName());
	}
	
	
	@Test
	public void testFindByCoordinatesUsingProperty(){
		Property one = property.findOne(1l);
		List<Province> findByCoordinates = provinces.findByCoordinates(one.getLat(), one.getLng());
		assertEquals(1, findByCoordinates.size());
	}

}
