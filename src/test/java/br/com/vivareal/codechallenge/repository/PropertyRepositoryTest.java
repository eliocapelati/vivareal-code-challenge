/**
 * 
 */
package br.com.vivareal.codechallenge.repository;

import java.math.BigDecimal;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.vivareal.codechallenge.domain.Property;
import br.com.vivareal.codechallenge.repository.PropertyRepository;

/**
 * @author eliocapelati
 *
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class PropertyRepositoryTest {

	@Autowired
	PropertyRepository property;
	Property p;

	@Before
	public void setup() {
		property.deleteAll();
		p = new Property().withBaths(3l).withBeds(3l).withDescription("with description").withLat(213).withLng(438)
				.withPrice(BigDecimal.ONE).withSquareMeters(50l)
				.withTitle("Awesome test2, the mission");
	}

	@Test
	public void testCreatingAndReading() {
		property.save(p);
		assertEquals(1, property.count());
		Property p2 = property.findOne(1l);
		assertEquals(p, p2);
	}
	
	@Test
	public void testfindByLatBetweenAndLngBetween(){
		property.save(p);
		Page<Property> page = property.findByLatBetweenAndLngBetween(200, 250, 400, 500, new PageRequest(0, 20));
		System.out.println(page);
		assertEquals(1, page.getNumberOfElements());
		assertEquals(p.getTitle(), page.getContent().get(0).getTitle());
	}
}
