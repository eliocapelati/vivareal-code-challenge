/**
 * 
 */
package br.com.vivareal.codechallenge.domain;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.vivareal.codechallenge.domain.Spotippos;

/**
 * @author eliocapelati
 *
 */
public class SpotipposTest {
	
	private final static String FILE_LOCATION = "src/test/resources/properties_1.json";
	private static Validator validator;
	private Spotippos spotippos;
	
	@Before
	public void setup() throws IOException {
		ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
		validator = vf.getValidator();
		ObjectMapper mapper = new ObjectMapper();
		spotippos = mapper.readValue(new File(FILE_LOCATION), Spotippos.class);
	}

	@Test
	public void testParseJson() {		
		assertNotNull(spotippos);
		assertEquals(1, spotippos.getProperties().size());
	}
	
	@Test
	public void testValidFromJson(){
		Set<ConstraintViolation<Spotippos>> validate = validator.validate(spotippos);
		assertTrue(validate.isEmpty());
	}

}
