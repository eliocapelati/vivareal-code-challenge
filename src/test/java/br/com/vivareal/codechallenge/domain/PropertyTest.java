/**
 * 
 */
package br.com.vivareal.codechallenge.domain;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.com.vivareal.codechallenge.domain.Property;

/**
 * @author eliocapelati
 *
 */
public class PropertyTest {

	private static Validator validator;
	private Property property;

	@BeforeClass
	public static void setupClass() {
		ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
		validator = vf.getValidator();
	}
	
	@Before
	public void setup(){
	    property = new Property()
				.withBaths(3l)
				.withBeds(3l)
				.withDescription("with description")
				.withLat(1)
				.withLng(2)
				.withPrice(BigDecimal.ONE)
				.withSquareMeters(50l)
				.withTitle("Awesome test");
	}

	@Test
	public void testOKProperty() {		
		Set<ConstraintViolation<Property>> zeroViolations = validator.validate(property);
		assertTrue(zeroViolations.isEmpty());
	}
	
	@Test
	public void testLatOutOfRange(){
		property.setLat(-1);
		Set<ConstraintViolation<Property>> constraintMin = validator.validate(property);
		assertEquals(1, constraintMin.size());
		
		property.setLat(1401);
		Set<ConstraintViolation<Property>> constraintViolations = validator.validate(property);
		assertEquals(1, constraintViolations.size());
	}
	
	@Test
	public void testLngOutOfRange(){
		property.setLng(-1);
		Set<ConstraintViolation<Property>> constraintMin = validator.validate(property);
		assertEquals(1, constraintMin.size());
		
		property.setLng(1001);
		Set<ConstraintViolation<Property>> constraintViolations = validator.validate(property);
		assertEquals(1, constraintViolations.size());
	}
	
	@Test
	public void testBedOutOfRange(){
		property.setBeds(0l);
		Set<ConstraintViolation<Property>> constraintMin = validator.validate(property);
		assertEquals(1, constraintMin.size());
		
		property.setBeds(6l);
		Set<ConstraintViolation<Property>> constraintViolations = validator.validate(property);
		assertEquals(1, constraintViolations.size());
	}
	
	@Test
	public void testBathOutOfRange(){
		property.setBaths(0l);
		Set<ConstraintViolation<Property>> constraintMin = validator.validate(property);
		assertEquals(1, constraintMin.size());
		
		property.setBaths(5l);
		Set<ConstraintViolation<Property>> constraintViolations = validator.validate(property);
		assertEquals(1, constraintViolations.size());
	}
	
	@Test
	public void testSquareMeterOutOfRange(){
		property.setSquareMeters(19l);
		Set<ConstraintViolation<Property>> constraintMin = validator.validate(property);
		assertEquals(1, constraintMin.size());
		
		property.setSquareMeters(241l);
		Set<ConstraintViolation<Property>> constraintViolations = validator.validate(property);
		assertEquals(1, constraintViolations.size());
	}

}
