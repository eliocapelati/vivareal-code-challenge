/**
 * 
 */
package br.com.vivareal.codechallenge.load;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.vivareal.codechallenge.boot.AppBootstrap;
import br.com.vivareal.codechallenge.load.LoadData;
import br.com.vivareal.codechallenge.repository.PropertyRepository;
import br.com.vivareal.codechallenge.repository.ProvinceRepository;

/**
 * The class {@link LoadData} is loaded by spring by {@link AppBootstrap}, and after the context is up the database is done for operation.
 * @author eliocapelati
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties=
							{"br.com.vivareal.codechallenge.properties.json.load.location=src/test/resources/properties_1.json",
							 "br.com.vivareal.codechallenge.province.json.load.location=src/test/resources/provinces_1.json"})
public class LoadDataTest {
	
	@Autowired
	private PropertyRepository property;
	@Autowired
	private ProvinceRepository province;

	@Test
	public void testLoadedIntoDatabase() throws IOException {
		assertEquals(1, property.count());
		assertEquals(1, province.count());
	}
}
